﻿//-----------------------------------------------------------------------
// <copyright file="Entity.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using Newtonsoft.Json;

    /// <summary>
    /// Default implementation of <see cref="IEntity" />, an object of the map which contains basic
    /// properties such as name and position.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Entity : IEntity
    {
        /// <summary>
        /// The queue of jobs to be performed on update.
        /// </summary>
        private readonly Queue<Job> jobQueue = new Queue<Job>();

        /// <summary>
        /// The friends who can call methods on us.
        /// </summary>
        private readonly List<IEntity> friends = new List<IEntity>();

        /// <summary>
        /// The parent.
        /// </summary>
        private IEntity parent;

        /// <summary>
        /// The update event handler.
        /// </summary>
        private EventHandler<EventArgs> updateEventHandler;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Entity(string name)
        {
            Contract.Requires(!string.IsNullOrEmpty(name));
            Name = name;
        }

        /// <summary>
        /// Occurs on update.
        /// </summary>
        private event EventHandler<EventArgs> UpdateEvent
        {
            add
            {
                // add the event handler if it hasn't already been added
                if (updateEventHandler == null || !updateEventHandler.GetInvocationList().Contains(value))
                {
                    updateEventHandler += value;

                    // subscribe to parent updates
                    SubscribeToUpdate();
                }
            }

            remove
            {
                updateEventHandler -= value;

                // if we have no subscribers, stop updating
                if (updateEventHandler?.GetInvocationList().Length == 0)
                    UnsubscribeToUpdate();
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty(Order = -2)]
        public string Name { get; }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>The parent.</value>
        public IEntity Parent
        {
            get
            {
                return parent;
            }

            set
            {
                if (parent == value)
                    return;

                // unsubscribe from any parent events
                UnsubscribeToUpdate();

                // remove from our current parent
                parent?.Children.Remove(this);

                // remove current parent as our friend
                this.RemoveFriend();

                parent = value;

                // add to our new parent
                if (parent != null)
                {
                    if (!parent.Children.Contains(this))
                        parent.Children.Add(this);

                    // add the parent as our friend
                    this.AddFriend(parent);
                }
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>The children.</value>
        [JsonProperty(Order = -2)]
        public IList<IEntity> Children { get; } = new List<IEntity>();

        /// <summary>
        /// Finds the child with the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>The child.</returns>
        public IEntity FindChild(string name)
        {
            return Children.FirstOrDefault(c => c.Name == name);
        }

        /// <summary>
        /// Performs the <see cref="Job" /> on the next update.
        /// </summary>
        /// <param name="job">The job.</param>
        public void PerformJobOnUpdate(Job job)
        {
            if (!this.CheckFriend(job.Source))
                throw new FriendException();

            jobQueue.Enqueue(job);

            // subscribe to parent updates so we can do this job on the next update
            SubscribeToUpdate();
        }

        /// <summary>
        /// Called when this instance should update.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        public void OnUpdate(object sender, EventArgs args)
        {
            // ensure update is being called by a friend
            if (!this.CheckFriend(sender))
                throw new FriendException();

            // call the protected update
            Update();

            // update subscribers
            if (updateEventHandler != null)
                updateEventHandler(this, args);
        }

        /// <summary>
        /// Subscribes a child to updates.
        /// </summary>
        /// <param name="child">The child.</param>
        public void SubscribeToUpdate(IEntity child)
        {
            if (!child.CheckFriend(this))
                throw new FriendException();

            if (Children.Contains(child))
                UpdateEvent += child.OnUpdate;
        }

        /// <summary>
        /// Unsubscribes a child from updates.
        /// </summary>
        /// <param name="child">The child.</param>
        public void UnsubscribeToUpdate(IEntity child)
        {
            if (!child.CheckFriend(this))
                throw new FriendException();

            if (Children.Contains(child))
                UpdateEvent -= child.OnUpdate;
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="string"/> that represents this instance.</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Subscribes to parent updates.
        /// </summary>
        protected void SubscribeToUpdate()
        {
            parent?.SubscribeToUpdate(this);
        }

        /// <summary>
        /// Unsubscribes from parent updates.
        /// </summary>
        protected void UnsubscribeToUpdate()
        {
            parent?.UnsubscribeToUpdate(this);
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        protected void Update()
        {
            // check if we have any jobs to do
            if (jobQueue.Count > 0)
            {
                // copy the queue to a temp one
                var jobs = new Queue<Job>(jobQueue);
                jobQueue.Clear();

                // perform actions in queue
                while (jobs.Count > 0)
                    PerformJob(jobs.Dequeue());
            }
            else
            {
                // since there were no actions to perform, unsubscribe if we have no subscribers
                if (updateEventHandler == null || updateEventHandler.GetInvocationList().Length == 0)
                {
                    UnsubscribeToUpdate();
                    return;
                }
            }
        }

        /// <summary>
        /// Performs the <see cref="Job"/>.
        /// </summary>
        /// <param name="job">The job.</param>
        /// <returns>Whether the job was performed successfully.</returns>
        protected virtual bool PerformJob(Job job)
        {
            return job.Perform();
        }
    }
}