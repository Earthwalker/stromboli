//-----------------------------------------------------------------------
// <copyright file="Query.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli
{
    /// <summary>
    /// A query tests whether the result of an operation between a source and a target object is true.
    /// Normally used in conjunction with <see cref="Job"/>.
    /// </summary>
    public abstract class Query
    {
        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public object Source { get; private set; }

        /// <summary>
        /// Gets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public object Target { get; private set; }

        /// <summary>
        /// Creates a new query.
        /// </summary>
        /// <typeparam name="T">The query type.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns>The new query.</returns>
        public static T Create<T>(object source, object target)
            where T : Query, new()
        {
            return new T() { Source = source, Target = target };
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <returns>Query result.</returns>
        public abstract bool Execute();
    }
}
