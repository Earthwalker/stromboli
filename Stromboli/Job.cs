//-----------------------------------------------------------------------
// <copyright file="Job.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli
{
    using System;

    /// <summary>
    /// A job is an <see cref="System.Action"/> that is performed by a source object on a target object.
    /// </summary>
    public sealed class Job
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Job" /> class.
        /// </summary>
        /// <param name="action">The action to perform.</param>
        /// <param name="source">The object performing the job.</param>
        /// <param name="target">The object being performed on.</param>
        private Job(Action action, object source, object target)
        {
            ContractExtensions.NotNull(action);

            Action = action;
            Source = source;
            Target = target;
        }

        /// <summary>
        /// Gets the action to perform.
        /// </summary>
        /// <value>
        /// The action to perform.
        /// </value>
        public Action Action { get; }

        /// <summary>
        /// Gets the object performing the job.
        /// </summary>
        /// <value>
        /// The object performing the job.
        /// </value>
        public object Source { get; }

        /// <summary>
        /// Gets the object being performed on.
        /// </summary>
        /// <value>
        /// The object being performed on.
        /// </value>
        public object Target { get; } 

        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        public Query Query { get; private set; }

        /// <summary>
        /// Creates a new instance of the <see cref="Job" /> class.
        /// </summary>
        /// <param name="action">The action to perform.</param>
        /// <param name="source">The object performing the job.</param>
        /// <param name="target">The object being performed on.</param>
        /// <returns>The new job.</returns>
        public static Job Create(Action action, object source, object target)
        {
            return new Job(action, source, target);
        }

        /// <summary>
        /// Creates a new instance of the <see cref="Job" /> class with a <see cref="Stromboli.Query"/>.
        /// </summary>
        /// <typeparam name="T">The <see cref="Query"/> type.</typeparam>
        /// <param name="action">The action to perform.</param>
        /// <param name="source">The object performing the job.</param>
        /// <param name="target">The object being performed on.</param>
        /// <returns>The new job.</returns>
        public static Job Create<T>(Action action, object source, object target)
            where T : Query, new()
        {
            // create the job
            var job = new Job(action, source, target);

            // create the query
            job.Query = Query.Create<T>(source, target);

            return job;
        }

        /// <summary>
        /// Performs this instance.
        /// </summary>
        /// <returns>Whether the job was completed successfully.</returns>
        public bool Perform()
        {
            // execute the query if there is one
            if (Query == null || Query.Execute())
            {
                try
                {
                    // execute the action
                    Action();
                    return true;
                }
                catch (FriendException)
                {
                    // if the action required execution by a friend, try calling it again
                    return Source?.CallOnFriend(Target, () => Action()) == true;
                }    
            }

            return false;
        }
    }
}
