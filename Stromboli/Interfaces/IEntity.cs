﻿//-----------------------------------------------------------------------
// <copyright file="IEntity.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Interface for objects in the map which contains basic properties such as name and position.
    /// </summary>
    [ContractClass(typeof(IEntityContract))]
    public interface IEntity
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        IEntity Parent { get; set; }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        IList<IEntity> Children { get; }

        /// <summary>
        /// Finds the child with the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>The child.</returns>
        IEntity FindChild(string name);

        /// <summary>
        /// Performs the <see cref="Job"/> on the next update.
        /// </summary>
        /// <param name="job">The job.</param>
        void PerformJobOnUpdate(Job job);

        /// <summary>
        /// Called when this instance should update.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        void OnUpdate(object sender, EventArgs args);

        /// <summary>
        /// Subscribes a child to updates.
        /// </summary>
        /// <param name="child">The child.</param>
        void SubscribeToUpdate(IEntity child);

        /// <summary>
        /// Unsubscribes a child from updates.
        /// </summary>
        /// <param name="child">The child.</param>
        void UnsubscribeToUpdate(IEntity child);
    }

    /// <summary>
    /// Contract class for <see cref="IEntity"/>.
    /// </summary>
    [ContractClassFor(typeof(IEntity))]
    public abstract class IEntityContract : IEntity
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public abstract IEntity Parent { get; set; }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public IList<IEntity> Children
        {
            get
            {
                Contract.Ensures(Children != null);
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Finds the child with the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>
        /// The child.
        /// </returns>
        public IEntity FindChild(string name)
        {
            Contract.Requires(!string.IsNullOrEmpty(name));
            throw new ArgumentNullException();
        }

        /// <summary>
        /// Performs the <see cref="Job" /> on the next update.
        /// </summary>
        /// <param name="job">The job.</param>
        public void PerformJobOnUpdate(Job job)
        {
            ContractExtensions.NotNull(job);
        }

        /// <summary>
        /// Called when this instance should update.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void OnUpdate(object sender, EventArgs args)
        {
            Contract.Requires(sender == Parent);
        }

        /// <summary>
        /// Subscribes a child to updates.
        /// </summary>
        /// <param name="child">The child.</param>
        public void SubscribeToUpdate(IEntity child)
        {
            ContractExtensions.NotNull(child);
        }

        /// <summary>
        /// Unsubscribes a child from updates.
        /// </summary>
        /// <param name="child">The child.</param>
        public void UnsubscribeToUpdate(IEntity child)
        {
            ContractExtensions.NotNull(child);
        }
    }
}