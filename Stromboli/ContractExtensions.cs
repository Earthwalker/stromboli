//-----------------------------------------------------------------------
// <copyright file="ContractExtensions.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;

/// <summary>
/// Extension methods to enhance Code Contracts and integration with Code Analysis.
/// </summary>
public static class ContractExtensions
{
    /// <summary>
    /// Validates the value is not null.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <exception cref="ArgumentNullException">The parameter cannot be null</exception>
    [ContractAbbreviator]
    [DebuggerStepThrough]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void NotNull(object value)
    {
        Contract.Requires(value != null, $"The parameter {nameof(value)} cannot be null");

        if (value == null)
            throw new ArgumentNullException(nameof(value), $"The parameter {nameof(value)} cannot be null");
    }
}