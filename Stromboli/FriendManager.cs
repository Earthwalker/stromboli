//-----------------------------------------------------------------------
// <copyright file="FriendManager.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli
{
    using System;
    using System.Collections.Concurrent;

    /// <summary>
    /// Manages object friends. Friends of an object can call certain methods on the object. 
    /// NOTE: Each object can only have one friend.
    /// </summary>
    public static class FriendManager
    {
        /// <summary>
        /// A dictionary of relationships. The value is the key's friend and can call friend-restricted methods on it.
        /// </summary>
        private static ConcurrentDictionary<object, object> friends = new ConcurrentDictionary<object, object>();

        /// <summary>
        /// A dictionary of approved threads. The value is the object the key thread has permission to call
        /// friend-restricted methods on.
        /// </summary>
        private static ConcurrentDictionary<int, object> threads = new ConcurrentDictionary<int, object>();

        /// <summary>
        /// Adds the friend.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="friend">The friend.</param>
        public static void AddFriend(this object source, object friend)
        {
            ContractExtensions.NotNull(source);
            ContractExtensions.NotNull(friend);

            friends.AddOrUpdate(source, friend, (key, value) => { return friend; });
        }

        /// <summary>
        /// Removes the friend.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The friend that was removed and null if no friend was removed.</returns>
        public static object RemoveFriend(this object source)
        {
            ContractExtensions.NotNull(source);

            object friend;
            friends.TryRemove(source, out friend);

            return friend;
        }

        /// <summary>
        /// Checks if the entity is a friend of the caller.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <returns>Whether the object is a friend.</returns>
        public static bool CheckFriend(this object source, object target)
        {
            ContractExtensions.NotNull(source);
            ContractExtensions.NotNull(target);

            // if source and target are the same object, return as a friend
            if (source.Equals(target))
                return true;

            object friend;
            friends.TryGetValue(source, out friend);

            if (friend == target)
                return true;

            return false;
        }

        /// <summary>
        /// Checks if the thread is an approved thread.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>Whether the thread has the necessary permissions.</returns>
        public static bool CheckThread(this object source)
        {
            ContractExtensions.NotNull(source);

            object target;
            threads.TryGetValue(Environment.CurrentManagedThreadId, out target);

            if (source == target)
                return true;

            return false;
        }

        /// <summary>
        /// Calls action on the specified target.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="action">The action.</param>
        /// <returns>Whether the call was performed.</returns>
        public static bool CallOnFriend(this object source, object target, Action action)
        {
            ContractExtensions.NotNull(source);
            ContractExtensions.NotNull(target);
            ContractExtensions.NotNull(action);

            // make sure the target is friends with the source
            if (CheckFriend(target, source))
            {
                threads.AddOrUpdate(Environment.CurrentManagedThreadId, target, (key, value) => { return target; });
                action();

                return true;
            }

            return false;
        }
    }
}
