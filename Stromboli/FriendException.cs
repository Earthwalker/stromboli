//-----------------------------------------------------------------------
// <copyright file="FriendException.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli
{
    using System;

    /// <summary>
    /// A <see cref="FriendException"/> is raised a friend-restricted method is called by a non-friend or on a unauthorized thread.
    /// </summary>
    public class FriendException : Exception
    {
    }
}