﻿//-----------------------------------------------------------------------
// <copyright file="JobTest.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class JobTest
    {
        [TestMethod]
        public void Create()
        {
            MyAssert.Throws<ArgumentNullException>(() => Job.Create(null, "source", "target"), "Null action");

            Action action = () => { };
            var job = Job.Create(action, "source", "target");
            Assert.AreEqual(job.Action, action, "Action failed to be set");
        }

        [TestMethod]
        public void Perform()
        {
            bool value = false;

            Action action = () => value = true;
            Assert.IsTrue(Job.Create(action, "source", "target").Perform(), "Failed to perform job with no query");
            Assert.IsTrue(Job.Create<ObjectsNotNullQuery>(action, value, value).Perform(), "Failed to perform job with true query");
            Assert.IsFalse(Job.Create<ObjectsNotNullQuery>(action, value, null).Perform(), "Performed job with false query");
        }
    }

    /// <summary>
    /// Returns if the objects are not null. Used for testing purposes.
    /// </summary>
    public class ObjectsNotNullQuery : Query
    {
        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Query result.</returns>
        public override bool Execute()
        {
            return Source != null && Target != null;
        }
    }
}
