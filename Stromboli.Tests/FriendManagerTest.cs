﻿//-----------------------------------------------------------------------
// <copyright file="FriendManagerTest.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class FriendManagerTest
    {
        [TestMethod]
        public void AddFriend()
        {
            ////MyAssert.Throws<ArgumentNullException>(() => FriendManager.AddFriend(null, new object()), "Null source");
            ////MyAssert.Throws<ArgumentNullException>(() => this.AddFriend(null), "Null friend");

            string friend = "friend";
            this.AddFriend(friend);
            Assert.IsTrue(this.CheckFriend(friend), "Failed to add friend");

            string friend2 = "friend2";
            this.AddFriend(friend2);
            Assert.IsTrue(this.CheckFriend(friend2), "Failed to replace friend");
        }

        [TestMethod]
        public void RemoveFriend()
        {
            ////MyAssert.Throws<ArgumentNullException>(() => FriendManager.RemoveFriend(null), "Null source");

            // TODO: finish implementation
        }

        [TestMethod]
        public void CheckFriend()
        {
            ////MyAssert.Throws<ArgumentNullException>(() => FriendManager.CheckFriend(null, new object()), "Null source");
            ////MyAssert.Throws<ArgumentNullException>(() => this.CheckFriend(null), "Null target");

            // TODO: finish implementation
        }

        [TestMethod]
        public void CheckThread()
        {
            ////MyAssert.Throws<ArgumentNullException>(() => FriendManager.CheckThread(null), "Null source");

            // TODO: finish implementation
        }

        [TestMethod]
        public void CallOnFriend()
        {
            Action action = () => { };
            ////MyAssert.Throws<ArgumentNullException>(() => FriendManager.CallOnFriend(null, new object(), action), "Null source");
            ////MyAssert.Throws<ArgumentNullException>(() => this.CallOnFriend(null, action), "Null target");
            ////MyAssert.Throws<ArgumentNullException>(() => this.CallOnFriend(new object(), null), "Null action");

            // TODO: finish implementation
        }
    }
}
