﻿//-----------------------------------------------------------------------
// <copyright file="EntityTest.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli.Tests
{
    using System;
    using System.Diagnostics.Contracts;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class EntityTest
    {
        [TestMethod]
        public void Entity()
        {
            string name = "entity";

            Assert.AreEqual(new Entity(name).Name, name, "Name not set");
            ////MyAssert.Throws<ArgumentNullException>(() => new Entity(null), "Null value");
            ////MyAssert.Throws<ArgumentNullException>(() => new Entity(string.Empty), "Empty string value");
        }

        [TestMethod]
        public void SetParent()
        {
            // create entity for testing
            var entity = new Entity("entity");

            // create parent entity
            var parent = new Entity("parent");

            entity.Parent = parent;

            Assert.IsTrue(entity.Parent == parent, "Failed to set the entity's parent");
            Assert.IsTrue(entity.CheckFriend(parent), "Failed to add parent as friend");
            Assert.IsTrue(parent.Children.Contains(entity), "Parent failed to add the child");
        }

        [TestMethod]
        public void FindChild()
        {
            // create entity for testing
            var entity = new Entity("entity");

            ////MyAssert.Throws<ArgumentNullException>(() => entity.FindChild(null), "Null value");
            ////MyAssert.Throws<ArgumentNullException>(() => entity.FindChild(string.Empty), "Empty string value");
            Assert.IsNull(entity.FindChild("non-child"), "Non-child");

            // create child entity
            var child = new Entity("child");

            child.Parent = entity;

            Assert.IsNotNull(entity.FindChild(child.Name), "Failed to find child");
        }

        [TestMethod]
        public void PerformJobOnUpdate()
        {
            // create entity for testing
            var entity = new Entity("entity");

            ////MyAssert.Throws<ArgumentNullException>(() => entity.PerformJobOnUpdate(null), "Null value");
            MyAssert.Throws<FriendException>(() => entity.PerformJobOnUpdate(Job.Create(() => { }, "source", "target")), "Non-friend");

            // call on parent
            var parent = new Entity("parent");
            entity.Parent = parent;

            parent.CallOnFriend(entity, () => entity.PerformJobOnUpdate(Job.Create(() => { }, entity, parent)));
        }

        [TestMethod]
        public void OnUpdate()
        {
            // create entity for testing
            var entity = new Entity("entity");

            ////MyAssert.Throws<ArgumentNullException>(() => entity.OnUpdate(null, EventArgs.Empty), "Null value");
            MyAssert.Throws<FriendException>(() => entity.OnUpdate(new Entity("non-friend"), EventArgs.Empty), "Non-friend");

            // call on parent
            var parent = new Entity("parent");
            entity.Parent = parent;

            parent.CallOnFriend(entity, () => entity.PerformJobOnUpdate(Job.Create(() => { }, entity, parent)));
        }

        [TestMethod]
        public void SubscribeToUpdate()
        {
            // create entity for testing
            var entity = new Entity("entity");

            ////MyAssert.Throws<ArgumentNullException>(() => entity.SubscribeToUpdate(null), "Null value");
            MyAssert.Throws<FriendException>(() => entity.SubscribeToUpdate(new Entity("non-friend")), "Non-friend");

            // call on child
            var child = new Entity("child");
            child.Parent = entity;

            entity.SubscribeToUpdate(child);
        }

        [TestMethod]
        public void UnsubscribeToUpdate()
        {
            // create entity for testing
            var entity = new Entity("entity");

            ////MyAssert.Throws<ArgumentNullException>(() => entity.UnsubscribeToUpdate(null), "Null value");
            MyAssert.Throws<FriendException>(() => entity.UnsubscribeToUpdate(new Entity("non-friend")), "Non-friend");

            // call on child
            var child = new Entity("child");
            child.Parent = entity;

            entity.UnsubscribeToUpdate(child);
        }
    }
}
