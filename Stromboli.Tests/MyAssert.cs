//-----------------------------------------------------------------------
// <copyright file="MyAssert.cs" company="Leamware">
//     Copyright (c) Leamware. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Stromboli.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    public static class MyAssert
    {
        public static void Throws<T>(Action func, string message) 
            where T : Exception
        {
            var exceptionThrown = false;
            try
            {
                func.Invoke();
            }
            catch (T)
            {
                exceptionThrown = true;
            }

            if (!exceptionThrown)
                throw new AssertFailedException($"{message}; an exception of type {typeof(T)} was expected, but not thrown");
        }
    }
}
