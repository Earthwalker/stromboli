# README #
Stromboli is a game engine framework written in C#. It features entities that can perform jobs at update intervals.

### Entity ###
The basic object that can perform jobs. Each entity can have one parent and multiple children. A parent is a friend of each child.

### Job ###
A job is an action that is performed by a source object on a target object.

### Query ###
A query tests whether the result of an operation between a source and a target object is true. Normally used in conjunction with Jobs.

### Friend ###
A friend is an object that can call certain methods on the object. Each object can only have one friend.

### Contribution guidelines ###

* StyleCop
* Code Contracts